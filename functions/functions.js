//import {postcodes} from './json/arrays.js';

export function getValues (data){
	var arr = [];
	for(var i in data){
	  for(var j in data[i]){
	  arr.push(data[i][j]);
		}
	}
	return arr;
}

export function covidPostcode(postcodes, covidData, covidDataToday, covidDataActive){
	var arr = [];
	
	for(let i = 0; i<postcodes.length; i++){
		var object = new Object();
		object.postcode = parseInt(postcodes[i]);
		object.numCasesToday = countPostcode(covidDataToday, postcodes[i]);
		object.numCasesActive = countPostcode(covidDataActive, postcodes[i]);
		object.numCases = countPostcode(covidData, postcodes[i]);
		arr.push(object);
	}
	return arr;
}

export function covidLGA(lgas, covidData, covidDataToday, covidDataActive){
	var arr = [];
	
	for(let i = 0; i<lgas.length; i++){
		var object = new Object();
		object.lga = lgas[i];
		object.numCasesToday = countLGA(covidDataToday, lgas[i]);
		object.numCasesActive = countLGA(covidDataActive, lgas[i]);
		object.numCases = countLGA(covidData, lgas[i]);
		arr.push(object);
	}
	return arr;
}

export function countPostcode (data, cpostcode) {
  let counter = 0;
  for (let i = 0; i<data.length; i++){
	if (data[i].postcode === cpostcode)
	counter++;
  }
  return counter;
}

export function countLGA (data, lga) {
  let counter = 0;
  for (let i = 0; i<data.length; i++){
	if (data[i].lga_name19 === lga)
	counter++;
  }
  return counter;
}
/**
const findDuplicates = (arr) => {
  let sorted_arr = arr.slice().sort(); // You can define the comparing function here. 
  // JS by default uses a crappy string compare.
  // (we use slice to clone the array so the
  // original array won't be modified)
  let results = [];
  for (let i = 0; i < sorted_arr.length - 1; i++) {
    if (sorted_arr[i + 1] == sorted_arr[i]) {
      results.push(sorted_arr[i]);
    }
  }
  return results;
}
**/

export function getDates (data) {
	
	var arr = [];
	for(let i = 0; i<data.length; i++){
		//console.log(data[i].notification_date);
			var object = new Object();
			object.date = data[i].notification_date;	
			arr.push(object);

	}
	return arr;
	
	
	 
		
}

export function countPostcodeCases(data){
	var arr = [];
	var returnArr = [];
	
		for(let i = 0; i<data.length; i++){
		//console.log(data[i].notification_date);
			//var object = new Object();
			//object.date = data[i].notification_date;	
			arr.push(data[i].notification_date);

	}
	
	//console.log("presort");
	//console.log(arr);
  let a = [],
    b = [],
    //arr = [...array], // clone array so we don't change the original when using .sort()
    prev;

  arr.sort();
  
  //console.log("sorted");
  //console.log(arr);
  for (let element of arr) {
    if (element !== prev) {
      a.push(element);
      b.push(1);
    }
    else ++b[b.length - 1];
    prev = element;
  }
  
  console.log("double array");
  
  for(let i = 0; i < a.length; i++){
	  //console.log(a[i] + " " + b[i]);
	  var object = new Object();
	  object.date = a[i];
	  object.cases = b[i];
	  returnArr.push(object);
  }

  return returnArr;
}

export function graph (data){
	var arr = [];
	for(let i = 0; i < data.length; i++){
	var arr2 = [];
	  arr2.push(data[i].date);
	  arr2.push(data[i].cases);
	  arr.push(arr2);

  }

	return arr;
	
}

