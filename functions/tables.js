import { StatusBar } from 'expo-status-bar';
import React, {component, useEffect, useMemo, useState } from 'react';
import { Platform, Modal, Pressable, Alert, AppRegistry, Button, Image, FlatList, StyleSheet, Text, View } from 'react-native';
import * as Location from 'expo-location';
import Constants from 'expo-constants';
import ReactDataGrid from '@inovua/reactdatagrid-community'
import '@inovua/reactdatagrid-community/index.css'
import NumberFilter from '@inovua/reactdatagrid-community/NumberFilter'
import SelectFilter from '@inovua/reactdatagrid-community/SelectFilter'

