import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState } from 'react';
import { Platform, Modal, Pressable, Alert, AppRegistry, Button, Image, FlatList, StyleSheet, Text, View } from 'react-native';
//import { Chart, Line, Area, HorizontalAxis, VerticalAxis } from 'react-native-responsive-linechart'
import { Chart } from "react-google-charts";

import {getDates} from './../functions/functions.js';
import {covidPostcode} from './../functions/functions.js';
import {countPostcodeCases} from './../functions/functions.js';
import {postcodes} from './../functions/arrays.js';
import {LGAs} from './../functions/arrays.js';
import {graph} from './../functions/functions.js'; 
//import log2 from './images.js';



export function CaseHistory({ navigation }) {
	
  const [isLoading, setLoading] = useState(true);
  const [dataCovid, setData] = useState([]);
  const [dataCovidToday, setData2] = useState([]);
  const [dataCovidActive, setData3] = useState([]);
  
  
  //console.log(dataCovid);
  //console.log(getValues(dataCovid));
  //console.log(count(dataCovid, "2121"));
  //console.log("postcode");
  //console.log(covidPostcode(postcodes, dataCovid));
  
  const dateT = new Date();
  const yearT = dateT.getFullYear();
  const monthT = dateT.getMonth();
  const dayT = dateT.getDate();
  const formattedDateT = (yearT + "-" + monthT + "-" + dayT);
  
  const date14 = new Date();
  date14.setTime(dateT.getTime()-1209600000);
  const year14 = date14.getFullYear();
  const month14 = date14.getMonth();
  const day14 = date14.getDate();
  const formattedDate14 = (year14 + "-" + month14 + "-" + day14);
  
  //console.log("totalCases");
  //console.log(totalCases);
  const postcodeSearch = '2220';
  const urlCovid = 'https://data.nsw.gov.au/data/api/3/action/datastore_search_sql?sql=SELECT * from "21304414-1ff1-4243-a5d2-f52778048b29" WHERE postcode LIKE\'' + postcodeSearch + '\'';
  const urlCovidToday = 'https://data.nsw.gov.au/data/api/3/action/datastore_search_sql?sql=SELECT * from "21304414-1ff1-4243-a5d2-f52778048b29" WHERE postcode LIKE\'' + postcodeSearch + '\' AND notification_date LIKE\'' + formattedDateT + '\'';
  const urlCovidActive = 'https://data.nsw.gov.au/data/api/3/action/datastore_search_sql?sql=SELECT * from "21304414-1ff1-4243-a5d2-f52778048b29" WHERE postcode LIKE\'' + postcodeSearch + '\' AND notification_date BETWEEN \'' + formattedDate14 + '\'AND\'' + formattedDateT + '\''
 
  const cases2 = (countPostcodeCases(dataCovid));
  console.log(urlCovid);
  console.log(urlCovidToday);
  console.log(urlCovidActive);
  console.log("cases2");
  //console.log(cases2);
  
  
  useEffect(() => {
    fetch(urlCovid)
      .then((response) => response.json())
      .then((json) => setData(json.result.records))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);
  
  
var data2 = (graph(cases2));
console.log("data2");
console.log(data2);



const data = [
    ["Date", "Cases" ],
	data2
 
];

const options = {
  chart: {
    title: "Covid Cases",
  },
  width: 900,
  height: 500,
  series: {
    // Gives each series an axis name that matches the Y-axis below.
    0: { axis: "Cases" },
  },
  axes: {
    // Adds labels to each axis; they don't have to match the axis names.
    y: {
      Cases: { label: "Cases" },
    },
  },
};


  return (
  <div className= "parent"> 
	
    <View style={{ flex: 4, alignItems: 'center', justifyContent: 'left',  backgroundColor: "green" }}>
		<Chart
      chartType="Line"
      width="100%"
      height="400px"
      data={data}
      options={options}
    />

	</View>	
	
  </div>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});
