import { StatusBar } from 'expo-status-bar';
//import React from 'react';
//import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import React, {component, useCallback, useEffect, useMemo, useState } from 'react';
import { Platform, Modal, Pressable, Alert, AppRegistry, Button, Image, FlatList, StyleSheet, Text, View } from 'react-native';
import * as Location from 'expo-location';
import Constants from 'expo-constants';


//import { Card } from 'react-native-paper';
//import MyCard from './card.js';
//import { DataTable } from 'react-native-paper';
//import {Table, TableWrapper, Row, Rows, Col, Cols, Cell} from 'react-native-table-component';
//import Table from 'react-native-simple-table';
import ReactDataGrid from '@inovua/reactdatagrid-community'
import '@inovua/reactdatagrid-community/index.css'
import NumberFilter from '@inovua/reactdatagrid-community/NumberFilter'
import SelectFilter from '@inovua/reactdatagrid-community/SelectFilter'
import { createNativeStackNavigator } from '@react-navigation/native-stack';

//import {useTable} from 'react-table'
import {covidPostcode} from './../functions/functions.js';
import {postcodes} from './../functions/arrays.js';
import {CaseHistory} from './../pages/CaseHistory.js';
//import {CovidPostcodeScreen} from './../pages/covidData.js';
//import {CovidLGAScreen} from './../pages/covidDataLGA.js';

export function CovidPostcodeScreen({ navigation }) {
	
  const Stack = createNativeStackNavigator();
  const [isLoading, setLoading] = useState(true);
  const [dataCovid, setData] = useState([]);
  const [dataCovidToday, setData2] = useState([]);
  const [dataCovidActive, setData3] = useState([]);
  
  
  //console.log(dataCovid);
  //console.log(getValues(dataCovid));
  //console.log(count(dataCovid, "2121"));
  //console.log("postcode");
  //console.log(covidPostcode(postcodes, dataCovid));
  
  const dateT = new Date();
  const yearT = dateT.getFullYear();
  const monthT = dateT.getMonth()+1;
  console.log("month t: " + monthT);
  const dayT = dateT.getDate();
  console.log("day t: " + dayT);
  var formattedDateT;
  if (monthT < 10){
	formattedDateT = (yearT + "-0" + monthT + "-" + dayT);
  }else if (dayT < 10){
	formattedDateT = (yearT + "-" + monthT + "-0" + dayT);
  }else if (monthT < 10 && dayT < 10){
	formattedDateT = (yearT + "-0" + monthT + "-0"+ dayT);
  }else{
	formattedDateT = (yearT + "-" + monthT + "-" + dayT);
  }
  
  
  //console.log("formattedDateT: " + formattedDateT)
  const date14 = new Date();
  date14.setTime(dateT.getTime()-1209600000);
  const year14 = date14.getFullYear();
  const month14 = date14.getMonth()+1;
  const day14 = date14.getDate();
  var formattedDate14;
  if (month14 < 10){
    formattedDate14 = (year14 + "-0" + month14 + "-" + day14);
  }else if (day14 < 10){
	formattedDate14 = (year14 + "-" + month14 + "-0" + day14);
  }else if (month14 < 10 && day14 < 10){
	formattedDate14 = (year14 + "-0" + month14 + "-0"+ day14);
  }else{
	formattedDate14 = (year14 + "-" + month14 + "-" + day14);
  }
  
  //console.log(formattedDateT);
  const formattedDateB = ("2020-01-01" );
  //console.log("totalCases");
  //console.log(totalCases);
  //21304414-1ff1-4243-a5d2-f52778048b29
  //https://data.nsw.gov.au/data/api/3/action/datastore_search?resource_id=5d63b527-e2b8-4c42-ad6f-677f14433520
  const urlCovid = 'https://data.nsw.gov.au/data/api/3/action/datastore_search_sql?sql=SELECT * from "5d63b527-e2b8-4c42-ad6f-677f14433520"'// WHERE notification_date BETWEEN \'' + formattedDateB + '\'AND\'' + formattedDateT + '\''
  const urlCovidToday = 'https://data.nsw.gov.au/data/api/3/action/datastore_search_sql?sql=SELECT * from "5d63b527-e2b8-4c42-ad6f-677f14433520" WHERE notification_date LIKE\'' + formattedDateT + '\'';
  const urlCovidActive = 'https://data.nsw.gov.au/data/api/3/action/datastore_search_sql?sql=SELECT * from "5d63b527-e2b8-4c42-ad6f-677f14433520" WHERE notification_date BETWEEN \'' + formattedDate14 + '\'AND\'' + formattedDateT + '\''
 
  const cases = (covidPostcode(postcodes, dataCovid, dataCovidToday, dataCovidActive));
  console.log(urlCovidToday);
  console.log(urlCovidActive);
  console.log("cases");
  console.log(cases);
  const columns = [
	{name: 'postcode', header:'Postcode', type: 'number', filterEditor: NumberFilter},
	{name: 'numCasesToday', header:'Todays Cases', type: 'number', filterEditor: NumberFilter},
	{name: 'numCasesActive', header:'Active Cases', type: 'number', filterEditor: NumberFilter},
	{name: 'numCases', header:'Total Cases', type: 'number', filterEditor: NumberFilter}
  ];
  const rows = cases;
  const gridStyle = {minHeight: 500, minWidth:600}
  const filterValue = [
  { name: 'postcode', operator: 'gte', type: 'number', value: 0},
  { name: 'numCasesToday', operator: 'gte', type: 'number', value: 0},
  { name: 'numCasesActive', operator: 'gte', type: 'number', value: 0},
  { name: 'numCases', operator: 'gte', type: 'number', value: 0}
];
  
  useEffect(() => {
    fetch(urlCovid)
      .then((response) => response.json())
      .then((json) => setData(json.result.records))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);
  
  useEffect(() => {
    fetch(urlCovidToday)
      .then((response) => response.json())
      .then((json) => setData2(json.result.records))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);
  
  useEffect(() => {
    fetch(urlCovidActive)
      .then((response) => response.json())
      .then((json) => setData3(json.result.records))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);
  
  const onRowDoubleClick = useCallback((rowProps) => {
	navigation.navigate('Case History')
	
	const locationPCLGA = rowProps.data.postcode;
	console.log('doubleclick' + locationPCLGA);
	//return locationPCLGA;
	//const goPlaces = navigation.navigate('Case History');
	//onPress={() => navigation.navigate('Case History')}
	
	
  }, [])

  const onRenderRow = useCallback((rowProps) => {
    // save the original handlers to be called later
    const { onDoubleClick } = rowProps;

    rowProps.onDoubleClick = (event) => {
      onRowDoubleClick(rowProps);
      if (onDoubleClick) {
        onDoubleClick(event);
		
      }
    };
 
  }, [])


  return (
	<div className= "parent"> 
		<View style={{ flexDirection: 'row', flex: 4, alignItems: 'center', justifyContent: 'center',  backgroundColor: "green" }}>
			<View style={styles.button}>
				<Button title="Postcodes" onPress={() => navigation.navigate('Covid Postcodes')} />
			</View>
			<View style={styles.button}>
				<Button title="LGAs" onPress={() => navigation.navigate('Covid LGA')} />
			</View>
		</View>	
		<View style={{alignItems: 'center', justifyContent: 'center', backgroundColor: "green"}}>
			<ReactDataGrid
			style={gridStyle}
			defaultFilterValue={filterValue}
			idProperty="postcode"
			columns={columns} 
			dataSource={rows} 
			sortable={true}
			onRenderRow={onRenderRow}/>
		</View>
	
		
	</div>
	

	
  );
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});
