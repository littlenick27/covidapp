import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import { useForm } from "react-hook-form";
import "./fromstyle.css";
import nameService from "./nameService.js";
export function Webform({ navigation }) {
  const { register, handleSubmit, watch, formState: { errors } } = useForm();
  const onSubmit = (data) => {
    alert(JSON.stringify(data));
	nameService.postNames(data).then((response) => {
            //setPeople	(response.data)
            //console.log(response.data);
        });
	
  };
  
   
  const onError = (errors, e) => console.log(errors, e);
  //console.log(watch("example")); // watch input value by passing the name of it

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
		<Text>First Name</Text>
      <input {...register("firstName", { required: true, maxLength: 20 })} />
		<Text>Last Name</Text>
      <input {...register("lastName", { required: true, pattern: /^[A-Za-z]+$/i })} />
		<Text>Age</Text>
      <input type="number" {...register("age", { required: true, min: 0, max: 150 })} />
	  <button type="submit">Submit</button>
    </form>
  );
}

