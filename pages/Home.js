import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState } from 'react';
import { Platform, Modal, Pressable, Alert, AppRegistry, Button, Image, FlatList, StyleSheet, Text, View } from 'react-native';
//import log2 from './images.js';



export function HomeScreen({ navigation }) {
	const [modalVisible, setModalVisible] = useState(false);
	React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Button onPress={() => navigation.navigate('Covid')} title="Go to Covid" />
      ),
    });
  }, []);
  
  
  return (
  <div className= "parent"> 
	 <View style={[styles.container, {
      // Try setting `flexDirection` to `"row"`.
      flexDirection: "row"
    }]}>
	 <View style={{ flex: 1, alignItems: 'left', justifyContent: 'left',  backgroundColor: "red" }}>
	 <Image source={require('./../assets/log2.png')}
	 style={{ width: 200, height: 200 }}

	 />

	</View>
	
    <View style={{ flex: 4, alignItems: 'center', justifyContent: 'left',  backgroundColor: "green" }}>
	
      <Text>Home Screen</Text>

	  <Button
        title="Go to Location"
        onPress={() => navigation.navigate('Location')}
      />

	   <Button
        title="Go to Covid Postcodes"
        onPress={() => navigation.navigate('Covid Postcodes')}
      />
	  
	  <Button
        title="Go to Covid LGA"
        onPress={() => navigation.navigate('Covid LGA')}
      />
	  
    </View>
	</View>	
	
	
	<View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Hello World!</Text>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => setModalVisible(!modalVisible)}
            >
              <Text style={styles.textStyle}>Hide Modal</Text>
            </Pressable>
          </View>
        </View>
      </Modal>
      <Pressable
        style={[styles.button, styles.buttonOpen]}
        onPress={() => setModalVisible(true)}
      >
        <Text style={styles.textStyle}>Show Modal</Text>
      </Pressable>
    </View>
	</div>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});
