import { StatusBar } from 'expo-status-bar';
import React, {component, useCallback, useEffect, useMemo, useState } from 'react';
import { Platform, Modal, Pressable, Alert, AppRegistry, Button, Image, FlatList, StyleSheet, Text, View } from 'react-native';
import * as Location from 'expo-location';
import Constants from 'expo-constants';



import ReactDataGrid from '@inovua/reactdatagrid-community'
import '@inovua/reactdatagrid-community/index.css'
import NumberFilter from '@inovua/reactdatagrid-community/NumberFilter'
import SelectFilter from '@inovua/reactdatagrid-community/SelectFilter'
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import axios from 'axios';

import nameService from "./nameService.js";

import { useForm } from "react-hook-form";
import "./fromstyle.css";

export function Webtable({ navigation }) {
	
  const Stack = createNativeStackNavigator();
  const [isLoading, setLoading] = useState(true);
  const [people, setData] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);

  const peoples = nameService.getNames();
   
  const { register, handleSubmit, watch, formState: { errors }, reset } = useForm();
  const onSubmit = (data) => {
    alert(JSON.stringify(data));
	nameService.putNames(data).then((response) => {
            //setPeople	(response.data)
            //console.log(response.data);
        });
	
  };
   
  const onDelete =  useCallback((rowProps) => {
	//console.log('rowProps: ' + rowProps.data.id);
    //alert(JSON.stringify(rowProps.data));
	nameService.deleteNames(rowProps.data.id).then((response) => {
            //setPeople	(response.data)
            //console.log(response.data);
        });
	
  }, []) 
  
  var currentValues = {
	id: -1,
    firstName: '',
    lastName: '',
    age: -1,
  };
  
  const columns = [
    {name: 'id', header:'Full Name', type: 'number', filterEditor: NumberFilter, render: ({ data }) => data.firstName + ' ' + data.lastName},
	{name: 'firstName', header:'First Name', type: 'string'},
	{name: 'lastName', header:'Last Name', type: 'string' },
	{name: 'age', header:'Age', type: 'number', filterEditor: NumberFilter},
	
  ];
  const rows = people;
  const gridStyle = {minHeight: 500, minWidth:600}
  const filterValue = [
  { name: 'id', operator: 'gte', type: 'number', value: 0},
  { name: 'firstName', operator: 'startsWith', type: 'string', value: ''},
  { name: 'lastName', operator: 'startsWith', type: 'string', value: ''},
  { name: 'age', operator: 'gte', type: 'number', value: 0},
 
];
 

  useEffect(() => {
    peoples
      .then ((response) => response.data)
	  .then((data) => {console.log(data);
	  setData(data);
	  })
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);
  
  
  const onRowDoubleClick = useCallback((rowProps) => {
	setModalVisible(true)

	const clickId = rowProps.data.id;
	const { onRowDelete } = rowProps;
	rowProps.onRowDelete = (event) => {
      onDelete(rowProps);
      if (onRowDelete) {
        onRowDelete(event);
		
     }
	
	
	
	
    };
	console.log('doubleclick ' + clickId);
	currentValues.id = rowProps.data.id;
    currentValues.firstName = rowProps.data.firstName;
    currentValues.lastName = rowProps.data.lastName;
    currentValues.age = rowProps.data.age;

	
  }, [])

  const onRenderRow = useCallback((rowProps) => {
    // save the original handlers to be called later
    const { onDoubleClick } = rowProps;
    rowProps.onDoubleClick = (event) => {
      onRowDoubleClick(rowProps);
      if (onDoubleClick) {
        onDoubleClick(event);
		
      }
    };
 
  }, [])


  return (
	<div className= "parent"> 
		<View style={{ flexDirection: 'row', flex: 4, alignItems: 'center', justifyContent: 'center',  backgroundColor: "green" }}>
		</View>	
		<View style={{alignItems: 'center', justifyContent: 'center', backgroundColor: "green"}}>
			<ReactDataGrid
			style={gridStyle}
			defaultFilterValue={filterValue}
			idProperty="Last Name"
			columns={columns} 
			dataSource={rows} 
			sortable={true}
			onRenderRow={onRenderRow}/>
		</View>
	
	  <View style={styles.centeredView}>
		  <Modal
			animationType="slide"
			transparent={true}
			visible={modalVisible}
			onRequestClose={() => {
			  Alert.alert("Modal has been closed.");
			  setModalVisible(!modalVisible);
			}}
		  >
			<View style={styles.centeredView}>
			  <View style={styles.modalView}>
					<form onSubmit={handleSubmit(onSubmit)}>
						<Text>First Name</Text>
						<input {...register("firstName",{ required: true, maxLength: 20 })} />
						<Text>Last Name</Text>
						<input {...register("lastName",{ required: true, pattern: /^[A-Za-z]+$/i })} />
						<Text>Age</Text>
						<input type="number" {...register("age",{ required: true, min: 0, max: 150 })} />
						<button type="submit">Submit</button>
					</form>
				<Pressable
				  style={[styles.button, styles.buttonClose]}
				  onPress={handleSubmit(onDelete)}
				>
				  <Text style={styles.textStyle}>Delete</Text>
				  
				</Pressable>
				
				<Pressable
				  style={[styles.button, styles.buttonClose]}
				  onPress={() => setModalVisible(!modalVisible)}
				>
				  <Text style={styles.textStyle}>Hide Modal</Text>
				</Pressable>
				
			  </View>
			</View>
		  </Modal>
		</View>
	
	</div>
	

	
  );
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});
