import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import {HomeScreen} from './pages/Home.js';
import {LocationScreen} from './pages/getLocation.js';
import {CovidPostcodeScreen} from './pages/covidData.js';
import {CovidLGAScreen} from './pages/covidDataLGA.js';
import {CaseHistory} from './pages/CaseHistory.js';
import {Webform} from './pages/webform.js';
import {Webtable} from './pages/webtable.js';
import 'react-native-gesture-handler';


const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <NavigationContainer>
		<Drawer.Navigator initialRouteName = "Home">
			<Drawer.Screen name = "Home" component = {HomeScreen}/>
			<Drawer.Screen name = "Location" component = {LocationScreen}/>
			<Drawer.Screen name = "Covid Postcodes" component = {CovidPostcodeScreen}/>
			<Drawer.Screen name = "Covid LGA" component = {CovidLGAScreen}/>
			<Drawer.Screen name = "Web Form" component = {Webform}/>
			<Drawer.Screen name = "Web Table" component = {Webtable}/>
		</Drawer.Navigator>
	</NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

//